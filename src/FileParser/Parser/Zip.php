<?php

namespace TextMedia\FileParser\Parser;

use \TextMedia\FileParser\Parser;
use \TextMedia\FileParser\ParserException;

/**
 * Парсер ZIP
 * Архив должен содержать только один файл, который будет распакован во временную директорию и обработан другими парсерами.
 */
class Zip extends AbstractParser
{
    /**
     * Собственно парсер.
     *
     * @param string $file Путь к файлу
     *
     * @throws \TextMedia\FileParser\ParserException
     *
     * @return string
     */
    protected static function parseFile(string $file): string
    {
        $zip  = self::openZipArchive($file);
        $name = $zip->statIndex(0);
        if (empty($name)) {
            $zip->close();
            throw new ParserException("Архив не содержит файлов: {$file}", ParserException::ARCHIVE_ERROR);
        } else {
            $name = $name['name'];
        }

        if (!empty($zip->statIndex(1))) {
            $zip->close();
            throw new ParserException("Архив должен содержать только один файл: {$file}", ParserException::ARCHIVE_ERROR);
        }

        $data = self::readZippedFile($zip, $name);
        $zip->close();

        try { // попробуем определить парсер по расширению
            $parser = Parser::getParserByFile($name, true);
        } catch (\Exception $ex) {
            unset($ex);
        }

        if (!empty($parser)) {
            return $parser::parse($data, false);
        }

        $temp = self::getTempFile($name);
        file_put_contents($temp, $data);

        try {
            $result = Parser::parse($temp);
            unlink($temp);
            return $result;
        } catch (\Exception $ex) {
            unlink($temp);
            throw $ex;
        }
    }
}
