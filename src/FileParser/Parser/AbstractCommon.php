<?php

namespace TextMedia\FileParser\Parser;

use \TextMedia\FileParser\ParserException;

use \SplFileInfo;

/**
 * Основной класс Parser и реализации парсинга Parser\* имеют общие методы,
 * которые не должны быть публичными, т.к. не имеют отношения к собственно парсингу,
 * т.е. к интерфейсу, поэтому выделил для них отдельного общего предка.
 */
abstract class AbstractCommon
{
    /**
     * Проверка файла на существование и возможность чтения.
     *
     * @param string $file Имя файла
     *
     * @throws \TextMedia\FileParser\ParserException
     */
    protected static function checkFile(string $file)
    {
        $fileInfo = new SplFileInfo($file);
        if (false === $fileInfo->getRealPath()) {
            throw new ParserException("Файл не найден: {$file}", ParserException::FILESYS_ERROR);
        }
        if (!$fileInfo->isFile()) {
            throw new ParserException("Это не файл: {$file}", ParserException::FILESYS_ERROR);
        }
        if (!$fileInfo->isReadable()) {
            throw new ParserException("Файл недоступен для чтения: {$file}", ParserException::FILESYS_ERROR);
        }
    }

    /**
     * Проверка возможности создать временный файл
     *
     * @param string $suffix OPTIONAL Суффикс
     *
     * @throws \TextMedia\FileParser\ParserException
     *
     * @return string Имя временного файла
     */
    protected static function getTempFile(string $suffix = null): string
    {
        $file = sys_get_temp_dir() . '/FileParser_'
              . md5(microtime()) . '_' . getmypid()
              . ($suffix ? "_{$suffix}" : '');
        if (false !== (new SplFileInfo($file))->getRealPath()) {
            self::checkFile($file);
            if (!(new SplFileInfo($file))->isWritable()) {
                throw new ParserException("Невозможно удалить файл: {$file}", ParserException::FILESYS_ERROR);
            }
            unlink($file);
        }
        return $file;
    }

    /**
     * Проверка существования консольной утилиты.
     *
     * @param string $command Команда.
     *
     * @throws \TextMedia\FileParser\ParserException
     *
     * @return string
     */
    protected static function checkCommand(string $command): string
    {
        static $commands = [];
        if (!array_key_exists($command, $commands)) {
            $info  = `whereis $command`;
            $match = [];
            $commands[$command] = preg_match("#{$command}:\s*(\S+)#", $info, $match) ? array_pop($match) : false;
        }

        if (empty($commands[$command])) {
            throw new ParserException("Команда не найдена: {$command}.", ParserException::CONSOLE_ERROR);
        }
        return $commands[$command];
    }

    /**
     * Выполнение команды в консоли и чтение результата из stdin.
     *
     * @throws \TextMedia\FileParser\ParserException
     *
     * @param string $command Команда.
     */
    protected static function execCommand(string $command): string
    {
        static $ignoreErrors = [
            'I can\'t find the name of your HOME directory',
        ];

        $descriptorspec = [
            0 => ['pipe', 'r'], // stdin
            1 => ['pipe', 'w'], // stdout
            2 => ['pipe', 'w'], // stderr
        ];
        $pipes   = [];
        $tempDir = sys_get_temp_dir();
        $process = proc_open("cd {$tempDir} && {$command}", $descriptorspec, $pipes);
        $result  = trim(stream_get_contents($pipes[1]));
        $errors  = trim(stream_get_contents($pipes[2]));
        fclose($pipes[1]);
        proc_close($process);

        if (!empty($errors) and !in_array($errors, $ignoreErrors)) {
            throw new ParserException("Ошибка выполнения команды {$command}: {$errors}", ParserException::CONSOLE_ERROR);
        }

        return (string) $result;
    }
}
