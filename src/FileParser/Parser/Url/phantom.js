var page   = require('webpage').create();
var system = require('system');

/**
 * Вывод результата в консоль и выход
 *
 * @param {String} result результат
 */
var ready = function (result) {
    if (result) {
        console.log(result);
    }
    phantom.exit();
};

// Необходимо как минимум 3 аргумента: Timeout, URL, UserAgent
if (system.args.length < 4) {
    system.stderr.writeLine('Usage phantom.js TIMEOUT "URL" "USERAGENT"');
    ready();
}

/**
 * Debug mode
 * @type {boolean}
 */
var debug = system.args.length == 5 && system.args[4];

/**
 * Список текущих запросов к ресурсам
 * @type {Array}
 */
var requests = [];

/**
 * Интервал проверки запросов к ресурсам
 */
var requestsInterval = 0;

/**
 * Что делать после загрузки страницы
 *
 * @param {String} status статус загрузки
 * @param {boolean} force
 */
var callbackOpenURL = function (status, force) {
    if (debug) {
        system.stderr.writeLine('ping requests: ' + requests.length);
    }

    //если есть активные запросы, крутимся в интервале и ждем пока запросы не закончатся
    if (requests.length && !force) {
        if (!requestsInterval) {
            requestsInterval = setInterval(function () {
                callbackOpenURL(status);
            }, 100);
        }
        return;
    }

    if (requestsInterval) { //если было ожидание, были и запросы.
        clearInterval(requestsInterval);
        requestsInterval = 0;
        if (!force) {
            return setTimeout(function () {
                //Нужно дать время на перестройку DOM
                callbackOpenURL(status);
            }, 200);
        }
    }/**/

    ready((status === 'success') ? page.evaluate(evaluateWebFunction) : null);
};

/**
 * Выполнение кода внутри страницы = получение кода всей страницы
 */
var evaluateWebFunction = function () {
    // удаляем стили/скрипты/фреймы и т.п.
    [
        'SCRIPT', 'STYLE', 'APPLET', 'OBJECT', 'PARAM', 'EMBED',
        'IMG', 'AUDIO', 'VIDEO', 'BGSOUND', 'CANVAS', 'TRACK', 'AREA', 'MAP',
        'INPUT', 'SELECT', 'TEXTAREA', 'BUTTON', 'OPTION', 'OPTGROUP',
        'IFRAME', 'FRAME', 'FRAMESET', 'NOINDEX', 'NOEMBED', 'NOSCRIPT', 'NOFRAMES'
    ].forEach(function (tag) {
        var nodes = document.getElementsByTagName(tag);
        while (nodes.length > 0) {
            nodes[0].parentNode.removeChild(nodes[0]);
        }
    });

    return document.getElementsByTagName('html')[0].outerHTML;
};

// Определим таймаут
var timeout = 1000 * parseInt(system.args[1]);

// Действие по таймауту открытия страницы
setTimeout(function () {
    if (debug) {
        system.stderr.writeLine('TIMEOUT. FORCE SUCCESS');
    }
    callbackOpenURL('success', true);
}, timeout);

// Действие по таймауту обработки страницы
setTimeout(ready, timeout + 2000);

// открываем страницу с нужным агентом
page.settings.userAgent = system.args[3];

page.onResourceRequested = function (request, r) {
    if (request['url'].match(/\.(css|gif|png|jpe?g|ico)$/i)) {
        r.abort();
        return;
    }

    if (request.headers && request.headers['Content-Type']
        && !request.headers['Content-Type'].match(/(text|java|json|xml)/)
    ) {
        r.abort();
        return;
    }

    requests.push(request.id);

    if (!debug) {
        return;
    }

    system.stderr.writeLine('= onResourceRequested()');
    system.stderr.writeLine('  request: ' + JSON.stringify(request, undefined, 4));
};

page.onResourceReceived = function (response) {
    var index = requests.indexOf(response.id);
    if (index >= 0) {
        requests.splice(index, 1);
    }

    if (!debug) {
        return;
    }

    system.stderr.writeLine('= onResourceReceived()');
    system.stderr.writeLine('  id: ' + response.id + ', stage: "' + response.stage + '", response: ' + JSON.stringify(response));
};

page.onResourceError = function (resourceError) {
    var index = requests.indexOf(resourceError.id);
    if (index >= 0) {
        requests.splice(index, 1);
    }

    if (!debug) {
        return;
    }

    system.stderr.writeLine('= onResourceError()');
    system.stderr.writeLine('  - id: "' + resourceError.id + '"');
    system.stderr.writeLine('  - unable to load url: "' + resourceError.url + '"');
    system.stderr.writeLine('  - error code: ' + resourceError.errorCode + ', description: ' + resourceError.errorString);
};

// отладка
if (debug) {
    page.onLoadStarted = function () {
        system.stderr.writeLine('= onLoadStarted()');
        var currentUrl = page.evaluate(function () {
            return window.location.href;
        });
        system.stderr.writeLine('  leaving url: ' + currentUrl);
    };

    page.onLoadFinished = function (status) {
        system.stderr.writeLine('= onLoadFinished()');
        system.stderr.writeLine('  status: ' + status);
    };

    page.onNavigationRequested = function (url, type, willNavigate, main) {
        system.stderr.writeLine('= onNavigationRequested');
        system.stderr.writeLine('  destination_url: ' + url);
        system.stderr.writeLine('  type (cause): ' + type);
        system.stderr.writeLine('  will navigate: ' + willNavigate);
        system.stderr.writeLine('  from page\'s main frame: ' + main);
    };
}

page.open(system.args[2], function (status, force) {
    setTimeout(function () {
        callbackOpenURL(status, force);
    }, 50);
});
