<?php

namespace TextMedia\FileParser\Parser;

/**
 * Парсер PDF
 */
class Pdf extends AbstractConsole
{
    /**
     * Параметры парсера
     *
     * @return array
     */
    protected static function getParams(): array
    {
        return [
            'pdftotext',
            '$command -q $file $result',
            null,
        ];
    }
}
