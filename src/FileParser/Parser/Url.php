<?php

namespace TextMedia\FileParser\Parser;

use \TextMedia\FileParser\Parser;
use \TextMedia\FileParser\ParserException;
use \TextMedia\MimeTypes\MimeTypes;

/**
 * Парсер URL (сперва скачиваем, потом обрабатываем через Parser).
 */
class Url extends AbstractCommon
{
    /** Указание не проверять mime-type при загрузке. */
    const NO_MIME_CHECK = '*';

    /** Указание скачивать только тексты. */
    const TEXTS_ONLY = '#^text/.*#';

    /** Опция скачивания: источник проксей (callable, возвращающий пару CURLOPT_PROXY + CURLOPT_PROXYUSERPWD). */
    const OPT_PROXIES_SOURCE = 'proxies-source';

    /** Опция скачивания: максимальное число редиректов. */
    const OPT_MAX_REDIRECTS = 'max-redirects';

    /** Опция скачивания: максимальное число проксей для перебора. */
    const OPT_MAX_PROXIES = 'max-proxies';

    /** Опция скачивания: таймаут (секунды). */
    const OPT_TIMEOUT = 'timeout';

    /** Опция скачивания: User-Agent. */
    const OPT_USERAGENT = 'user-agent';

    /** Опция скачивания: mime-type или расширение. */
    const OPT_MIME_TYPE = 'mime-type';

    /** Опция скачивания: не добавлять в URL якорь ("#…"). */
    const OPT_NO_ANCHORS = 'no-anchors';

    /** Опция скачивания: без PhantomJS (только для HTML). */
    const OPT_NO_PHANTOMJS = 'no-phantomjs';

    /** UserAgent по умолчанию */
    const DEFAULT_USERAGENT = 'TextMediaSiteBot';

    /** @var array Опции по умолчанию. */
    protected static $defaults = [
        self::OPT_PROXIES_SOURCE => false,
        self::OPT_MAX_REDIRECTS  => 5,
        self::OPT_MAX_PROXIES    => 5,
        self::OPT_TIMEOUT        => 10,
        self::OPT_USERAGENT      => false,
        self::OPT_MIME_TYPE      => false,
        self::OPT_NO_ANCHORS     => false,
        self::OPT_NO_PHANTOMJS   => true,
    ];

    /** @var array Информация по последнему запросу */
    protected static $lastRequest = [];

    /**
     * Собственно парсер.
     *
     * @param string $url    URL.
     * @param array $options OPTIONAL Опции загрузки (см. $defaults).
     *
     * @return string
     */
    public static function parse(string $url, array $options = []): string
    {
        // сперва скачаем содержимое файла
        $content = self::loadContent($url, $options);

        // определим расширение из имени файла
        $type = MimeTypes::getFileExtension(parse_url($content['url'], PHP_URL_PATH));

        // если не удалось - по mime-типу из curl-а
        if (empty($type) and !empty($content['mime-type'])) {
            $mimes = MimeTypes::getExtenstionsByMime($content['mime-type']);
            if (0 !== count($mimes)) {
                $type = reset($mimes);
            }
        }

        // сохраним во временный файл и обработаем его парсером
        $temp = self::getTempFile($type ? ".{$type}" : '');
        file_put_contents($temp, $content['result']);
        try {
            $result = Parser::parse($temp);
            unlink($temp);
            return $result;
        } catch (\Exception $ex) {
            unlink($temp);
            throw $ex;
        }
    }

    /**
     * Загрузка содержимого по URL.
     *
     * @param string $url    URL.
     * @param array $options OPTIONAL Опции загрузки (см. $defaults).
     *
     * @throws \TextMedia\FileParser\ParserException
     *
     * @return array
     */
    public static function loadContent(string $url, array $options = []): array
    {
        self::$lastRequest = [
            'url'      => $url,
            'options'  => $options,
            'requests' => [],
        ];

        if (!self::checkUrlValid($url)) {
            throw new ParserException("Передан неправильный URL: {$url}.", ParserException::URL_ERROR);
        }

        $options = array_replace(self::$defaults, $options);
        foreach ($options as $name => $value) {
            self::checkOptionValue($name, $value);
        }
        if (empty($options[self::OPT_MIME_TYPE]) or !is_string($options[self::OPT_MIME_TYPE])) {
            $options[self::OPT_MIME_TYPE] = Parser::getAvailableMimes();
        } elseif (self::NO_MIME_CHECK === $options[self::OPT_MIME_TYPE]) {
            $options[self::OPT_MIME_TYPE] = false;
        } elseif (@preg_match($options[self::OPT_MIME_TYPE], '') !== false) {
            // nothing to do
        } elseif (preg_match('#.+/.+#', $options[self::OPT_MIME_TYPE])) {
            $options[self::OPT_MIME_TYPE] = (array) $options[self::OPT_MIME_TYPE];
        } else {
            $options[self::OPT_MIME_TYPE] = MimeTypes::getMimesByExtension($options[self::OPT_MIME_TYPE]);
        }

        $mimeType = $httpCode = null;
        $redirects = [];
        $proxies = [empty($options[self::OPT_PROXIES_SOURCE]) ? [] : call_user_func($options[self::OPT_PROXIES_SOURCE])];
        $headRequest = true;
        $httpEncoding = '';
        $url = self::urlPrepare($url, null, $options[self::OPT_NO_ANCHORS]);
        $gmt = date('r', (time() + 19360000) * 1000);
        $cookies = "beget=begetok; expires={$gmt}; path=/";
        do {
            $curlOptions = array_replace([
                CURLOPT_URL            => $url,
                CURLOPT_TIMEOUT        => $options[self::OPT_TIMEOUT],
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER         => true,
                CURLOPT_ENCODING       => $httpEncoding,
                CURLOPT_NOBODY         => $headRequest,
                CURLOPT_HTTPGET        => !$headRequest,
                CURLOPT_POST           => false,
                CURLOPT_HTTPHEADER     => ['Expect:'],
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_USERAGENT      => $options[self::OPT_USERAGENT] ?: self::DEFAULT_USERAGENT,
                CURLOPT_COOKIE         => $cookies,
                CURLOPT_FRESH_CONNECT  => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
            ], end($proxies));

            $curl = curl_init();
            curl_setopt_array($curl, $curlOptions);
            $response = (string) curl_exec($curl);
            $curlRersponse = [
                'options'      => $curlOptions,
                'error'        => curl_error($curl),
                'errno'        => curl_errno($curl),
                'headers'      => curl_getinfo($curl),
                'body_headers' => [],
            ];
            curl_close($curl);
            $httpCode = (int) ($curlRersponse['headers']['http_code'] ?? 0);

            // HEAD запрос не поддерживается 405 Method Not Allowed
            if (405 === $httpCode) {
                if ($headRequest) {
                    array_push(self::$lastRequest['requests'], $curlRersponse);
                    $headRequest = false;
                    continue;
                } else {
                    throw new ParserException('Не удалось загрузить файл - метод запрещён.', ParserException::URL_ERROR);
                }
            }

            // ошибка кодировки - меняем ее и повторям запрос
            if ($curlRersponse['errno'] == 23 or $curlRersponse['errno'] == 61) {
                if (empty($httpEncoding) and preg_match('#unencoding#', $curlRersponse['error'])) {
                    array_push(self::$lastRequest['requests'], $curlRersponse);
                    $httpEncoding = 'identity';
                    continue;
                } else {
                    throw new ParserException('Не удалось загрузить файл - ошибка определения кодировки.', ParserException::URL_ERROR);
                }
            }

            // распарсим заголовки ответа и получим тело, если есть
            $response = trim(preg_replace('#^HTTP/1\.\d\s200\sConnection\sestablished#i', '', $response));
            $response = explode("\r\n\r\n", $response, 2);
            $headers  = explode("\r\n", trim(array_shift($response)));
            while ($headers) {
                $line = trim(array_shift($headers));
                if (empty($curlRersponse['body_headers'])) {
                    $curlRersponse['body_headers']['http_code'] = $line;
                } elseif (strpos($line, ':')) {
                    list($name, $value) = explode(':', $line, 2);
                    $curlRersponse['body_headers'][$name] = trim($value);
                    if (strtolower($name) === 'x-generated-by' && $curlRersponse['body_headers'][$name] === 'UMI.CMS') {
                        $cookies = '';
                    }
                }
            }
            $curlRersponse['result'] = trim(array_shift($response));
            array_push(self::$lastRequest['requests'], $curlRersponse);

            // проверим ошибки прокси: получим новый или попробуем без прокси
            if (!empty($options[self::OPT_PROXIES_SOURCE]) and (407 === $httpCode or 503 === $httpCode or
                    $curlRersponse['errno'] == 7)) {
                if ($options[self::OPT_MAX_PROXIES] <= count($proxies)) {
                    $options[self::OPT_PROXIES_SOURCE] = false;
                    $proxies = [[]];
                } else {
                    array_push($proxies, call_user_func($options[self::OPT_PROXIES_SOURCE]));
                }
                continue;
            }

            array_push($redirects, $url);
            $baseURL = $url;

            // проверим редиректы
            $redirect = false;
            if (!empty($curlRersponse['headers']['redirect_url'])) {
                $redirect = $curlRersponse['headers']['redirect_url'];
            } elseif (!empty($curlRersponse['body_headers']['Location'])) {
                $redirect = $curlRersponse['body_headers']['Location'];
            }
            if (!empty($redirect)) {
                $url = self::urlPrepare($redirect, $baseURL, $options[self::OPT_NO_ANCHORS]);
                if (in_array($url, $redirects)) {
                    throw new ParserException('Обнаружена циклическая переадресация.', ParserException::URL_ERROR);
                }
                if (count($redirects) > $options[self::OPT_MAX_REDIRECTS]) {
                    throw new ParserException('Достигнуто максимальное число редиректов.', ParserException::URL_ERROR);
                }
                $hostOld = preg_replace('#^www\.#', '', parse_url($baseURL, PHP_URL_HOST));
                $hostNew = preg_replace('#^www\.#', '', parse_url($url, PHP_URL_HOST));
                if ($hostOld !== $hostNew) {
                    throw new ParserException('Обнаружена кросс-доменная переадресация.', ParserException::URL_ERROR);
                }
                continue;
            }

            if (200 !== $httpCode) {
                throw new ParserException("Не удалось загрузить файл - ошибка №{$httpCode}.", ParserException::URL_ERROR);
            }

            // проверим mime-тип; если норм и не html - загрузим файл
            $mimeType = empty($curlRersponse['headers']['content_type']) ? null
                : preg_replace('#;.*$#', '', $curlRersponse['headers']['content_type']);
            if (empty($mimeType)) {
                if (!empty($options[self::OPT_MIME_TYPE])) {
                    throw new ParserException('Не удалось определить content-type.', ParserException::URL_ERROR);
                }
            } elseif (is_string($options[self::OPT_MIME_TYPE])) {
                if (!preg_match($options[self::OPT_MIME_TYPE], $mimeType)) {
                    throw new ParserException("Не проддерживаемый content-type: {$mimeType}.", ParserException::URL_ERROR);
                }
            } elseif (is_array($options[self::OPT_MIME_TYPE])) {
                if (!in_array($mimeType, $options[self::OPT_MIME_TYPE])) {
                    throw new ParserException("Не проддерживаемый content-type: {$mimeType}.", ParserException::URL_ERROR);
                }
            }

            // Если это был HEAD-запрос и тип контента не-HTML или PhantomJs отключен - выкачиваем данные по ссылке.
            if ($headRequest and ('text/html' !== $mimeType or !empty($options[self::OPT_NO_PHANTOMJS]))) {
                $headRequest = false;
                continue;
            } else {
                break;
            }
        } while (true);
        $curlRersponse['url'] = $url;
        $curlRersponse['mime-type'] = $mimeType;
        $curlRersponse['http-code'] = $httpCode;

        // html обработаем phantom-ом, если не указана опция OPT_NO_PHANTOMJS
        $isHTML = ('text/html' === $mimeType);
        if ($isHTML and !empty($options[self::OPT_NO_PHANTOMJS])) {
            $curlRersponse['result'] = self::htmlPrepare((string) ($curlRersponse['result'] ?? ''));
        } elseif ($isHTML) {
            $curlRersponse['phantomjs'] = true;
            $phantom   = self::checkCommand('phantomjs');
            $proxy     = empty($p = end($proxies)) ? '' : sprintf('--proxy=%s --proxy-auth=%s ', $p[CURLOPT_PROXY], $p[CURLOPT_PROXYUSERPWD]);
            $timeout   = $options[self::OPT_TIMEOUT];
            $userAgent = $options[self::OPT_USERAGENT] ?: self::DEFAULT_USERAGENT;
            $command   = "timeout 30s {$phantom}"
                . ' --ssl-protocol=@@@ --ignore-ssl-errors=true --disk-cache=no --max-disk-cache-size=0 --load-images=no'
                . " {$proxy}" . __DIR__ . '/Url/phantom.js'
                . " {$timeout} \"{$url}\" \"{$userAgent}\"";
            $sslTypes = ['any', 'tlsv1', 'sslv3'];
            while (!empty($sslTypes) and empty($curlRersponse['result'])) {
                $exec = str_replace('@@@', array_shift($sslTypes), $command);
                array_push(self::$lastRequest['requests'], $exec);
                $curlRersponse['result'] = self::execCommand($exec);
            }
        }

        if (empty($curlRersponse['result'])) {
            throw new ParserException("Не удалось получить данные по ссылке: {$url}.", ParserException::URL_ERROR);
        }

        return $curlRersponse;
    }

    /**
     * Получение информации по последней загрузке.
     *
     * @return array
     */
    public static function getLastRequest(): array
    {
        return self::$lastRequest;
    }

    /**
     * Установка значений опций по умолчанию.
     *
     * @param string $name  Имя опции.
     * @param mixed  $value Значение.
     */
    public static function setDefaultOption(string $name, $value)
    {
        self::checkOptionValue($name, $value);
        self::$defaults[$name] = $value;
    }

    /**
     * Проверка, правильный ли URL.
     *
     * @param string $url URL.
     *
     * @return boolean
     */
    public static function checkUrlValid(string $url): bool
    {
        return (preg_match('#^(ftp|http|https)://#i', $url)
            and !empty(parse_url($url, PHP_URL_HOST)));
    }

    /**
     * "Нормализация" URL - декодирование IDN, QUERY_STRING и проч.
     *
     * @param string  $url       URL.
     * @param boolean $noAnchors OPTIONAL Не добавлять в URL якорь ("#…").
     *
     * @throws \TextMedia\FileParser\ParserException
     *
     * @return string
     */
    public static function normalizeUrl(string $url, bool $noAnchors = false): string
    {
        if (!self::checkUrlValid($url)) {
            throw new ParserException("Передан неправильный URL: {$url}.", ParserException::URL_ERROR);
        }

        $parsed = parse_url($url);
        if ($noAnchors and !empty($parsed['fragment'])) {
            unset($parsed['fragment']);
        }

        return self::urlConstruct($parsed, true);
    }

    /**
     * Подготовка URL к запросу.
     *
     * @param string  $location  Полный или относительный URL.
     * @param boolean $baseURL   OPTIONAL Базовый URL.
     * @paran boolean $noAnchors OPTIONAL Не добавлять в URL якорь ("#…").
     *
     * @return string
     */
    public static function urlPrepare(string $location, string $baseURL = null, $noAnchors = false): string
    {
        if (empty($location)) {
            return $baseURL;
        }

        $parsed = parse_url($location);
        if (!empty($baseURL)) {
            $baseURL = parse_url($baseURL);
            $parts   = ['scheme', 'user', 'pass', 'host', 'port'];
            if ('#' === $location{0}) {
                array_push($parts, 'query');
            } elseif ('?' === $location{0}) {
                array_push($parts, 'path');
            } elseif (!empty($parsed['path']) and '/' !== $parsed['path']{0}) {
                $path = empty($baseURL['path']) ? '/' : $baseURL['path'];
                if ('/' !== $path and !preg_match('#/$#', $path)) {
                    $path = dirname($path);
                }
                $parsed['path'] = rtrim($path, '/') . '/' . $parsed['path'];
            }
            foreach ($parts as $part) {
                if (empty($parsed[$part])) {
                    $parsed[$part] = array_key_exists($part, $baseURL) ? $baseURL[$part] : null;
                }
            }
        }

        if ($noAnchors and !empty($parsed['fragment'])) {
            unset($parsed['fragment']);
        }

        return self::urlConstruct($parsed, false);
    }

    /**
     * Проверка значений опций.
     *
     * @param string $name  Имя опции.
     * @param mixed  $value Значение.
     *
     * @throws \TextMedia\FileParser\ParserException
     */
    protected static function checkOptionValue(string $name, $value)
    {
        $valid = (array_key_exists($name, self::$defaults) and $value === self::$defaults[$name]);
        if (!$valid) {
            switch ($name) {
                case self::OPT_PROXIES_SOURCE:
                    $valid = (false === $value or is_callable($value));
                    break;
                case self::OPT_TIMEOUT:
                case self::OPT_MAX_REDIRECTS:
                    $valid = (is_int($value) and $value > 0);
                    break;
                case self::OPT_MAX_PROXIES:
                    $valid = (is_int($value) and $value >= 0);
                    break;
                case self::OPT_USERAGENT:
                case self::OPT_MIME_TYPE:
                    $valid = (false === $value or is_string($value));
                    break;
                case self::OPT_NO_ANCHORS:
                case self::OPT_NO_PHANTOMJS:
                    $valid = is_bool($value);
                    break;
            }
        }
        if (!$valid) {
            throw new ParserException("Неправильное значение '{$name}'.", ParserException::URL_ERROR);
        }
    }

    /**
     * Сборка URL из массива, полученного при помощи parse_url().
     *
     * @param array   $parsed    Массив.
     * @param boolean $normalize Кодировать в UTF или в RAW
     *
     * @return string
     */
    protected static function urlConstruct(array $parsed, bool $normalize): string
    {
        static $utfDec = ['а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', 'ё', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'Ё'];
        static $utfEnc = ['%D0%B0', '%D0%B1', '%D0%B2', '%D0%B3', '%D0%B4', '%D0%B5', '%D0%B6', '%D0%B7', '%D0%B8', '%D0%B9', '%D0%BA', '%D0%BB', '%D0%BC', '%D0%BD', '%D0%BE', '%D0%BF', '%D1%80', '%D1%81', '%D1%82', '%D1%83', '%D1%84', '%D1%85', '%D1%86', '%D1%87', '%D1%88', '%D1%89', '%D1%8A', '%D1%8B', '%D1%8C', '%D1%8D', '%D1%8E', '%D1%8F', '%D1%91', '%D0%90', '%D0%91', '%D0%92', '%D0%93', '%D0%94', '%D0%95', '%D0%96', '%D0%97', '%D0%98', '%D0%99', '%D0%9A', '%D0%9B', '%D0%9C', '%D0%9D', '%D0%9E', '%D0%9F', '%D0%A0', '%D0%A1', '%D0%A2', '%D0%A3', '%D0%A4', '%D0%A5', '%D0%A6', '%D0%A7', '%D0%A8', '%D0%A9', '%D0%AA', '%D0%AB', '%D0%AC', '%D0%AD', '%D0%AE', '%D0%AF', '%D0%81'];

        $parts  = ['scheme', 'user', 'pass', 'host', 'port', 'path', 'query', 'fragment'];
        $parts  = array_fill_keys($parts, '');
        $parsed = array_replace($parts, $parsed);

        if (('https' === $parsed['scheme'] and 443 == $parsed['port'])
            or ('http' === $parsed['scheme'] and 80 == $parsed['port'])
        ) {
            $parsed['port'] = '';
        }
        if (!empty($parsed['user'])) {
            $parsed['auth'] = $parsed['user']
                . (empty($parsed['pass']) ? '' : (':' . $parsed['pass']));
        }

        foreach (['path', 'query', 'fragment'] as $part) {
            if (!empty($parsed[$part])) {
                if ($normalize) {
                    $parsed[$part] = rawurldecode(trim($parsed[$part]));
                    $parsed[$part] = str_replace('%20', ' ', $parsed[$part]);
                } else {
                    // избавляемся от HTML-сущностей и кодируем utf-символы
                    $parsed[$part] = html_entity_decode(trim($parsed[$part]));
                    $parsed[$part] = str_replace(' ', '%20', $parsed[$part]);
                    $parsed[$part] = str_replace($utfDec, $utfEnc, $parsed[$part]);
                }
            }
        }

        return strtolower($parsed['scheme']) . '://'
            . (empty($parsed['auth']) ? '' : ($parsed['auth'] . '@'))
            . ($normalize
                ? idn_to_utf8(mb_strtolower($parsed['host']), IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46)
                : idn_to_ascii(mb_strtolower($parsed['host']), IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46))
            . (empty($parsed['port']) ? '' : (':' . $parsed['port']))
            . (empty($parsed['path']) ? '/' : $parsed['path'])
            . (empty($parsed['query']) ? '' : ('?' . $parsed['query']))
            . (empty($parsed['fragment']) ? '' : ('#' . $parsed['fragment']));
    }

    /**
     * Обработка скачанного HTML.
     *
     * @param string $html Скачанный HTML.
     *
     * @return string
     */
    protected static function htmlPrepare(string $html): string
    {
        return $html;
    }
}
