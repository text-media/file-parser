<?php

namespace TextMedia\FileParser\Parser;

use \TextMedia\EncodingConverter\Detector;
use \TextMedia\FileParser\Parser\Html\Readability;

use \DOMDocument;
use \DOMNode;
use \DOMXPath;

/**
 * Парсер HTML
 */
class Html extends AbstractParser
{
    /**
     * Чтение "/html/head/title".
     *
     * @param string $html HTML.
     *
     * @return string
     */
    public static function getTitle(string $html): string
    {
        $charset = Detector::detectByHtmlMeta($html);
        $html = self::getConverter()->encode($html, 'UTF-8', $charset);
        $title = (new DOMXPath(self::getDom($html)))->query('/html/head/title');
        return $title->length ? $title->item(0)->nodeValue : '';
    }

    /**
     * Парсер исходного кода.
     *
     * @param string $data HTML.
     *
     * @return string
     */
    protected static function parseText(string $data): string
    {
        // быстрая очистка исходного кода
        $html = self::cleanFast($data);

        // ищем текст по тэгам TEXT.RU
        if (!empty($text = self::parseByTextMediaTag($html))) {
            return $text;
        }

        // глубокая очистка исходного кода
        $html = self::cleanDeep($html);

        // ищем текст при помощи Readability
        $text   = '';
        $points = 3;
        while (empty($text) && $points < 6) {
            $text = self::parseByReadability($html, $points);
            $points++;
        }
        return $text;
    }

    /**
     * Собственно парсер.
     *
     * @param string $file Путь к файлу.
     *
     * @return string
     */
    protected static function parseFile(string $file): string
    {
        return self::parseText(file_get_contents($file));
    }

    /**
     * Ищем текст по тэгам TEXT.RU.
     *
     * @param string $html HTML.
     *
     * @return string
     */
    protected static function parseByTextMediaTag(string $html): string
    {
        return preg_match_all('#<!--\s*TEXT\.?(?:RU)?\s*-->(.*?)<!--\s*/\s*TEXT\.?(?:RU)?\s*-->#isu', $html, $blocks, PREG_PATTERN_ORDER)
                ? self::prepareText(implode(PHP_EOL, array_filter(array_map([self::class, 'prepareText'], $blocks[1]))))
                : '';
    }

    /**
     * Ищем текст при помощи Readability.
     *
     * @param string  $html   HTML.
     * @param integer $points Глубина поиска.
     *
     * @return string
     */
    protected static function parseByReadability(string $html, int $points): string
    {
        if (function_exists('tidy_parse_string')) {
            $tidy = tidy_parse_string($html, ['wrap' => 0], 'UTF8');
            $tidy->cleanRepair();
            $html = $tidy->value;
        }

        $readability = new Readability($html);
        $readability->points = $points;
        $readability->debug  = defined('TESTING');
        $readability->convertLinksToFootnotes = false;
        $result = $readability->init();
        if (!$result) {
            return '';
        }

        $text = $readability->getContent()->innerHTML;
        if (function_exists('tidy_parse_string')) {
            $tidy = tidy_parse_string($text, ['indent' => true, 'show-body-only' => true, 'wrap' => 0], 'UTF8');
            $tidy->cleanRepair();
            $text = $tidy->value;
        }
        return self::prepareText($text);
    }

    /**
     * Быстрая очистка исходного кода.
     *
     * @param string $html HTML.
     *
     * @return string
     */
    protected static function cleanFast(string $html): string
    {
        if (strpos($html, 'Your SEO optimized title')) {
            /** @see https://wordpress.org/support/topic/text-suddenly-appeared-above-header#post-4473561 */
            $html = preg_replace('#</?meta(|\s[^>]*)>#is', '', $html);
            $html = preg_replace('#<body>\s+page\scontents\s+</body>#is', '', $html);
            $html = preg_replace('#<title>[^<]*</title>#is', '', $html);
            $html = preg_replace('#<head>\s*</head>#is', '', $html);
            $html = preg_replace('#<html>\s*</html>#is', '', $html);
        }

        $html = preg_replace('#^.*?<html#is', '<html', $html);
        $html = preg_replace('#<\!--\s*(/)?\s*(noindex)\s*-->#is', '<$1$2>', $html);
        $html = preg_replace('#<div id="system-debug" class="profiler">.*</body>#is', '</body>', $html);

        $charset = Detector::detectByHtmlMeta($html);
        $html = self::getConverter()->encode($html, 'UTF-8', $charset);

        $cleanTags = [
            'HEAD', 'SCRIPT', 'STYLE', 'APPLET', 'OBJECT', 'PARAM', 'EMBED',
            'IMG', 'AUDIO', 'VIDEO', 'BGSOUND', 'CANVAS', 'TRACK',
            'INPUT', 'SELECT', 'TEXTAREA', 'BUTTON', 'OPTION', 'OPTGROUP',
            'IFRAME', 'FRAME', 'FRAMESET', 'NOINDEX', 'NOEMBED', 'NOSCRIPT', 'NOFRAMES',
            'THEAD', 'TFOOT', 'COMMENT', 'MENU', 'NAV', 'AREA', 'MAP'
        ];

        array_walk($cleanTags, function (string $tag) use (&$html) {
            $html = self::removeTags($html, $tag);
        });

        $dom = self::getDom($html);
        self::removeNodes($dom, function (DOMNode $node) use ($cleanTags) {
            return in_array(strtoupper($node->tagName), $cleanTags);
        });

        return trim($dom->saveHTML());
    }

    /**
     * Глубокая очистка исходного кода.
     *
     * @param string $html HTML.
     *
     * @return string
     */
    protected static function cleanDeep(string $html): string
    {
        // костыль для какого-то сайта, чтобы Readability не игнорировал…
        $html = strtr($html, [
            'extra-content'       => 'content',
            'layout-sidebar-left' => 'layout-left',
            'sortament-content'   => 'sortament',
            'sortament-item'      => 'sortament',
        ]);

        $dom   = self::getDom($html);
        $xpath = new DOMXPath($dom);
        foreach ($xpath->query('//comment()') as $node) {
            $node->parentNode->removeChild($node);
        }

        /** @var string[] $skipNodes список узлов, которые нельзя удалять */
        $skipNodes = ['HTML', 'BODY'];

        /** @var string $regexpStyle регулярка для удаления невидимых узлов */
        $regexpStyle = '#(display\s*:\s*none|visibility\s*:\s*hidden)#is';

        /** @var string регулярка для удаления узлов с определенным className/ID */
        $regexpCheck = '#('
                . 'rss|pag[ei]nation|pager|popup|comment(?!able)|contact|search-result'
                . '|sortament|userForm|combx|community|disqus|shoutbox|remark|twitter|tweet|ad-break|agregate'
                . '|combx|commercial|innerad|footnote|related|sponsor|tags(?!\-none)'
                . '|(log|sign)(in|out)|registe?r|b(read|[\-_])crumb|button'
            . ')#i';

        self::removeNodes($dom, function (DOMNode $node) use ($skipNodes, $regexpStyle, $regexpCheck) {
            if (in_array(strtoupper($node->tagName), $skipNodes)) {
                return false;
            }

            if (!is_null($node->attributes->getNamedItem('hidden'))) {
                return true;
            }

            $style = $node->getAttribute('style');
            if ($style && preg_match($regexpStyle, $style)) {
                return true;
            } else {
                $node->removeAttribute('style');
            }

            $check = trim($node->getAttribute('class') . ' ' . $node->getAttribute('id'));
            if ($check && preg_match($regexpCheck, $check)) {
                return true;
            }

            return false;
        });

        return trim($dom->saveHTML());
    }

    /**
     * Получение страницы в виде объекта.
     *
     * @param string $html HTML.
     *
     * @return \DOMDocument
     */
    protected static function getDom(string $html): DOMDocument
    {
        static $meta = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
        $errors = error_reporting(0);
        $dom    = new DOMDocument('1.0', 'utf-8');
        $dom->loadHTML("{$meta}{$html}", LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);
        error_reporting($errors);
        return $dom;
    }

    /**
     * Очистка DOM от лишних узлов.
     *
     * @param \DOMDocument $dom          DOM.
     * @param callable     $verification Функция проверки, нужно ли удалять узел:
     *                                   аргумент - DOMNode, результат - boolean.
     */
    protected static function removeNodes(DOMDocument $dom, callable $verification)
    {
        do {
            $deletions = 0;
            foreach ($dom->getElementsByTagName('*') as $node) {
                if (call_user_func($verification, $node)) {
                    $node->parentNode->removeChild($node);
                    $deletions++;
                }
            }
        } while ($deletions > 0);
    }

    /**
     * Очистка HTML от лишних узлов.
     *
     * @param string $html HTML.
     * @param string $tag  Тэг.
     *
     * @return string
     */
    protected static function removeTags(string $html, string $tag): string
    {
        while (true) {
            $parts1 = preg_split("#<\s*{$tag}#is", $html, 2);
            if (count($parts1) !== 2) {
                break;
            }

            $parts2 = preg_split("#<\s*/{$tag}\s*>#is", $parts1[1], 2);
            if (count($parts2) !== 2) {
                break;
            }

            $html = "{$parts1[0]}{$parts2[1]}";
        }
        return $html;
    }
}
