<?php

namespace TextMedia\FileParser\Parser;

use TextMedia\FileParser\Parser;
use TextMedia\FileParser\ParserException;

/**
 * Парсинг изображения (абстракция).
 */
abstract class AbstractImage extends AbstractConsole
{
    /** Поддерживаемые типы изображений. */
    const SUPPORTED_TYPES = [
        IMAGETYPE_GIF  => Parser::GIF_IMAGE,
        IMAGETYPE_JPEG => Parser::JPEG_IMAGE,
        IMAGETYPE_PNG  => Parser::PNG_IMAGE,
        IMAGETYPE_BMP  => Parser::BMP_IMAGE,
    ];

    /**
     * {@inheritdoc}
     */
    protected static function parseFile(string $file): string
    {
        $imageInfo = getimagesize($file);
        if (empty($imageInfo) || empty($imageInfo[2])) {
            throw new ParserException("Файл '{$file}' не является изображением.", ParserException::PARSE_ERROR);
        }
        if (!isset(self::SUPPORTED_TYPES[$imageInfo[2]])) {
            throw new ParserException("Неподдерживаемый тип изображения '{$file}'.", ParserException::PARSE_ERROR);
        }

        return parent::parseFile($file);
    }

    /**
     * {@inheritdoc}
     */
    protected static function getParams(): array
    {
        return [
            'tesseract',
            '$command -l rus+eng $file stdout 2>/dev/null',
            null,
        ];
    }
}
