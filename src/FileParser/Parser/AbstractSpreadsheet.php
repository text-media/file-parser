<?php

namespace TextMedia\FileParser\Parser;

use PhpOffice\PhpSpreadsheet\Exception as PhpSpreadsheetException;
use PhpOffice\PhpSpreadsheet\IOFactory;

use TextMedia\FileParser\ParserException;

/**
 * Парсинг электронных таблиц (абстракция).
 */
abstract class AbstractSpreadsheet extends AbstractParser
{
    /**
     * {@inheritdoc}
     */
    protected static function parseFile(string $file): string
    {
        try {
            $reader = IOFactory::createReader(IOFactory::identify($file));
            $reader->setReadDataOnly(true);
            $sheets = $reader->load($file);
            $data   = $reader->listWorksheetInfo($file);
        } catch (PhpSpreadsheetException $ex) {
            throw new ParserException("Ошибка парсинга электронной таблицы '{$file}'.", ParserException::PARSE_ERROR, $ex);
        }

        $result = [];
        foreach ($sheets->getAllSheets() as $num => $sheet) {
            array_push($result, $sheet->getTitle());
            for ($row = 0; $row < $data[$num]['totalRows']; $row++) {
                $line = [];
                for ($col = 0; $col < $data[$num]['totalColumns']; $col++) {
                    $cell  = $sheet->getCellByColumnAndRow($col, $row);
                    $value = trim($cell->getCalculatedValue());
                    if ('' !== $value) {
                        array_push($line, $value);
                    }
                }
                if (!empty($line)) {
                    array_push($result, implode(' ', $line));
                }
            }
        }
        return implode(PHP_EOL, $result);
    }
}
