<?php

namespace TextMedia\FileParser\Parser;

/**
 * Парсер DOCX
 */
class Docx extends AbstractZipped
{
    /**
     * Параметры обработки ZippedXml
     *
     * @return array
     */
    protected static function getParams(): array
    {
        return ['word/document.xml', null];
    }
}
