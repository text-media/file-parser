<?php

namespace TextMedia\FileParser\Parser;

/**
 * Парсер TXT
 */
class Txt extends AbstractParser
{
    /**
     * Собственно парсер.
     *
     * @param string $data Содержимое файла
     *
     * @return string
     */
    protected static function parseText(string $data): string
    {
        return $data; // тупо отдаём обратно
    }

    /**
     * Собственно парсер.
     *
     * @param string $file Путь к файлу
     *
     * @return string
     */
    protected static function parseFile(string $file): string
    {
        return file_get_contents($file); // тупо читаем файл
    }
}
