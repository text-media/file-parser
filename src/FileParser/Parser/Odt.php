<?php

namespace TextMedia\FileParser\Parser;

/**
 * Парсер ODT
 */
class Odt extends AbstractZipped
{
    /**
     * Параметры обработки ZippedXml
     *
     * @return array
     */
    protected static function getParams(): array
    {
        return ['content.xml', null];
    }
}
