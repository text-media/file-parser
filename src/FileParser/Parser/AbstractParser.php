<?php

namespace TextMedia\FileParser\Parser;

use \TextMedia\EncodingConverter\Converter;
use \TextMedia\EncodingConverter\Detector;
use \TextMedia\FileParser\ParserException;

use \SimpleXMLElement;
use \ZipArchive;

/**
 * Парсинг файла (абстракция)
 */
abstract class AbstractParser extends AbstractCommon
{
    /**
     * Парсинг файла
     *
     * @param string $source Путь к файлу или его содержимое
     * @param string $file   OPTIONAL Передано имя файла (по умолчанию) или его содержимое
     *
     * @throws \TextMedia\FileParser\ParserException
     *
     * @return string
     */
    public static function parse(string $source, bool $file = true): string
    {
        if ($file) {
            self::checkFile($source);
            $source = static::parseFile(realpath($source));
        } else {
            $source = static::parseText($source);
        }
        $text   = strtr(self::getConverter()->encode($source), "\r", '');
        $parts  = explode("\n", (string) html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
        $result = trim(implode("\n", array_filter(array_map(function ($line) {
            return trim(preg_replace('#\s\s+#u', ' ', $line));
        }, $parts))));

        if (empty($result)) {
            throw new ParserException('Не удалось найти в файле текст');
        }
        return $result;
    }

    /**
     * Получение alias-ов данного типа файла (всех возможных расширений).
     * По умолчанию возввращает пустой массив, но может быть переопределено в потомках.
     *
     * @return array
     */
    public static function getAliases(): array
    {
        return [];
    }

    /**
     * Собственно парсер. Для каждого типа файла должна быть собственная реализация в потомках.
     *
     * @param string $file Путь к файлу
     *
     * @return string
     */
    abstract protected static function parseFile(string $file): string;

    /**
     * Парсинг исходного текста файла.
     * По умолчанию, данные сохраняются в файл, после чего парсятся методом parseFile,
     * но это поведение может быть переопределено - например, для html это необязательно.
     *
     * @param string $data Содержимое файла
     *
     * @return string
     */
    protected static function parseText(string $data): string
    {
        $ext  = strtolower(substr(get_called_class(), strlen(__NAMESPACE__) + 1));
        $temp = self::getTempFile(".{$ext}");
        file_put_contents($temp, $data);
        try {
            $result = static::parseFile($temp);
            unlink($temp);
            return $result;
        } catch (\Exception $ex) {
            unlink($temp);
            throw $ex;
        }
    }

    /**
     * Конвертер кодировок
     *
     * @return \TextMedia\EncodingConverter\Converter
     */
    protected static function getConverter(): Converter
    {
        return new Converter;
    }

    /**
     * Преобразование XML в SimpleXMLElement
     *
     * @param string $xml XML
     *
     * @throws \TextMedia\FileParser\ParserException
     *
     * @return \SimpleXMLElement
     */
    protected static function getXMLElement(string $xml): SimpleXMLElement
    {
        $errors = error_reporting();
        error_reporting(0);
        try {
            $charset = Detector::detectByXmlDeclare($xml);
            $xml = self::getConverter()->encode($xml, 'UTF-8', (string) $charset);
            $xml = preg_replace('#^<\?xml[^>]*>#', '', $xml);
            $xml = new SimpleXMLElement($xml);
            error_reporting($errors);
            return $xml;
        } catch (\Exception $ex) {
            error_reporting($errors);
            throw $ex;
        }
    }

    /**
     * Подготовка текста (добавление/удаление пробелов, вырезка тэгов и проч.)
     *
     * @param string $text
     *
     * @return string
     */
    protected static function prepareText(string $text): string
    {
        $text = strip_tags(str_replace('<', ' <', $text));
        $text = htmlspecialchars(html_entity_decode($text), ENT_NOQUOTES);
        $text = preg_replace('#[ \t\r]+#', ' ', $text);
        $text = preg_replace('#\s?\n+\s*#s', "\n", $text);
        $text = preg_replace('#\s+([\.\?!:;,…])#u', '\\1', $text);
        return trim($text);
    }

    /**
     * Открытие архива
     *
     * @param string $file Путь к файлу
     *
     * @throws \TextMedia\FileParser\ParserException
     *
     * @return \ZipArchive
     */
    protected static function openZipArchive(string $file): ZipArchive
    {
        $zip = new ZipArchive;
        if (true !== $zip->open($file)) {
            throw new ParserException("Не удалось открыть zip-архив: {$file}", ParserException::ARCHIVE_ERROR);
        }
        return $zip;
    }

    /**
     * Получение содержимого файла из архива
     *
     * @param ZipArchive $zip  Архив
     * @param string     $name Имя файла
     *
     * @throws \TextMedia\FileParser\ParserException
     *
     * @return string
     */
    protected static function readZippedFile(ZipArchive $zip, string $name): string
    {
        $index = $zip->locateName($name);
        if (false === $index) {
            $zip->close();
            throw new ParserException("Не удалось найти файл в архиве: {$name}", ParserException::ARCHIVE_ERROR);
        }
        return $zip->getFromIndex($index);
    }
}
