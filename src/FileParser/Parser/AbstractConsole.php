<?php

namespace TextMedia\FileParser\Parser;

/**
 * Абстракция для парсеров файлов консольными утилитами (doc, rtf, pdf).
 */
abstract class AbstractConsole extends AbstractParser
{
    /**
     * Собственно парсер.
     *
     * @param string $file Путь к файлу
     *
     * @return string
     */
    protected static function parseFile(string $file): string
    {
        list($command, $template, $callback) = static::getParams();
        $exec = strtr($template, ['$file' => "\"{$file}\"", '$command' => self::checkCommand($command)]);
        $text = (false === strpos($exec, '$result'))
                ? self::execCommand($exec)
                : self::convertFile($exec);
        return is_callable($callback)
                ? call_user_func($callback, $text)
                : $text;
    }

    /**
     * Конвертирование файла в новый файл
     *
     * @param string $command Шаблон команды, в который ещё нужно подставить имя файла результата
     */
    protected static function convertFile(string $command): string
    {
        $file = self::getTempFile('.txt');
        self::execCommand(strtr($command, ['$result' => "\"{$file}\""]));
        self::checkFile($file);
        $text = file_get_contents($file);
        unlink($file);
        return $text;
    }

    /**
     * Парсинг консольными утилитами всегда сводится к одному:
     * выполнить команду, прочитать данные из stdin или нового файла,
     * и, возможно, как-то дополнительно их обработать.
     *
     * Выходной индексированный массив должен содержать 3 элемента:
     *  - название утилиты, например "convert";
     *  - шаблон вызова утилиты, например "$command -in $file -out $result";
     *  - NULL или функцию для дополнительной обработки текста.
     *
     * @return array
     */
    abstract protected static function getParams(): array;
}
