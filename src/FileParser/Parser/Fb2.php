<?php

namespace TextMedia\FileParser\Parser;

/**
 * Парсер Fictopn Book
 */
class Fb2 extends AbstractParser
{
    /**
     * Парсер исходного кода
     *
     * @param string $data XML
     *
     * @return string
     */
    protected static function parseText(string $data): string
    {
        $xml = self::getXMLElement($data);
        $xml->registerXPathNamespace('fb', 'http://www.gribuser.ru/xml/fictionbook/2.0');
        foreach ($xml->xpath('//fb:body') as $body) {
            if (empty($body->attributes()->name)) {
                $xml = $body->asXML();
                break;
            }
        }
        if (!is_string($xml)) {
            return '';
        }

        $text = preg_replace('#</(section|annotation|epigraph|title|p|poem|stanza|v)>#iu', "</\\1>\n", $xml);
        return self::prepareText($text);
    }

    /**
     * Собственно парсер.
     *
     * @param string $file Путь к файлу
     *
     * @return string
     */
    protected static function parseFile(string $file): string
    {
        return self::parseText(file_get_contents($file));
    }
}
