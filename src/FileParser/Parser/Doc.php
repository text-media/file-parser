<?php

namespace TextMedia\FileParser\Parser;

/**
 * Парсер DOC
 */
class Doc extends AbstractConsole
{
    /**
     * Параметры парсера
     *
     * @return array
     */
    protected static function getParams(): array
    {
        return [
            'antiword',
            'LANG=en_US.utf-8 $command $file',
            null
        ];
    }
}
