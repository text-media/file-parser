<?php

namespace TextMedia\FileParser\Parser;

use \TextMedia\FileParser\ParserException;

use \SimpleXMLElement;

/**
 * Парсер EPUB
 */
class Epub extends AbstractZipped
{
    /**
     * Параметры обработки ZippedXml
     *
     * @throws \TextMedia\FileParser\ParserException
     *
     * @return array
     */
    protected static function getParams(): array
    {
        $file = func_get_arg(0);
        $zip  = self::openZipArchive($file);

        $info = self::getXMLElement(self::readZippedXML($zip, 'META-INF/container.xml'));
        $info->registerXPathNamespace('x', $info->getNamespaces()['']);
        $root = $info->xpath('//x:rootfile');
        if (0 === count($root)) {
            $zip->close();
            throw new ParserException("Не удалось получить 'rootfile' файла {$file}", ParserException::PARSE_ERROR);
        }

        $path = (string) reset($root)->attributes()->{'full-path'};
        if (empty($path)) {
            $zip->close();
            throw new ParserException("Не удалось получить 'full-path' файла {$file}", ParserException::PARSE_ERROR);
        }

        $content = self::getXMLElement(self::readZippedXML($zip, $path));
        $content->registerXPathNamespace('x', $content->getNamespaces()['']);
        $items = array_fill_keys(array_map(function (SimpleXMLElement $item) {
            return (string) $item->attributes()->idref;
        }, $content->xpath('//x:spine/x:itemref')), '');
        if (0 === count($items)) {
            $zip->close();
            throw new ParserException("Не удалось получить 'itemref' файла {$file}", ParserException::PARSE_ERROR);
        }

        $zip->close();
        $path = dirname($path);
        foreach ($content->xpath('//x:manifest/x:item') as $item) {
            $attrs = $item->attributes();
            $idref = (string) $attrs->id;
            if (array_key_exists($idref, $items)) {
                $items[$idref] = preg_replace('#^\./#', '', "{$path}/{$attrs->href}");
            }
        }
        return [$items, function ($text) {
            try {
                return Html::parse($text, false);
            } catch (\Exception $ex) {
                unset($ex);
                return '';
            }
        }];
    }
}
