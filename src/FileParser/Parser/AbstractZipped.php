<?php

namespace TextMedia\FileParser\Parser;

use \DOMDocument;
use \ZipArchive;

/**
 * Абстракция для парсеров файлов, представленных в виде сжатого Zip-архива (docx, odt)
 */
abstract class AbstractZipped extends AbstractParser
{
    /**
     * Собственно парсер.
     *
     * @param string $file Путь к файлу
     *
     * @return string
     */
    protected static function parseFile(string $file): string
    {
        $zip   = self::openZipArchive($file);
        $texts = [];
        list($names, $callback) = static::getParams($file);
        foreach ((array) $names as $name) {
            $text = self::readZippedXML($zip, $name);

            foreach (['</text:p>', '</w:p>'] as $tag) {
                $text = str_replace($tag, "{$tag}\n", $text);
            }

            array_push($texts, is_callable($callback)
                ? call_user_func($callback, $text)
                : strip_tags($text));
        }
        $zip->close();
        return implode("\n", array_filter($texts));
    }

    /**
     * Получение содержимого файла из архива
     *
     * @param ZipArchive $zip  Архив
     * @param string     $name Имя файла
     *
     * @return string
     */
    protected static function readZippedXML(ZipArchive $zip, string $name): string
    {
        $data   = self::readZippedFile($zip, $name);
        $errors = error_reporting(0);
        $xml    = new DOMDocument;
        $xml->loadXML($data, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);
        $text   = $xml->saveXML();
        error_reporting($errors);
        return $text;
    }

    /**
     * Для парсера важно только имя файла внутри архива - для каждого типа файла оно разное.
     * Т.о. потомки должны только определять его в данном методе (может быть несколько).
     *
     * Кроме того, возможно, потребуется дополнительная пост-обработка
     * - callback для этого так же определяется данным методом.
     *
     * @return array
     */
    abstract protected static function getParams(): array;
}
