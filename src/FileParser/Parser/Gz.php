<?php

namespace TextMedia\FileParser\Parser;

use \TextMedia\FileParser\Parser;
use \TextMedia\FileParser\ParserException;

/**
 * Парсер GZIP
 * Имя архива должно иметь вид: "<оригинальное_имя>.gz"
 */
class Gz extends AbstractParser
{
    /**
     * Собственно парсер.
     *
     * @param string $file Путь к файлу
     *
     * @return string
     */
    protected static function parseFile(string $file): string
    {
        $name = substr($file, 0, -3);
        try { // попробуем определить парсер по расширению
            $parser = Parser::getParserByFile($name, true);
        } catch (ParserException $ex) {
            unset($ex);
        }

        $source = gzdecode(file_get_contents($file));
        if (!empty($parser)) {
            return $parser::parse($source, false);
        }

        $temp = self::getTempFile(basename($name));
        file_put_contents($temp, $source);
        try {
            $result = Parser::parse($temp);
            unlink($temp);
            return $result;
        } catch (\Exception $ex) {
            unlink($temp);
            throw $ex;
        }
    }

    /**
     * Собственно парсер.
     *
     * @param string $data Содержимое файла
     *
     * @return string
     */
    protected static function parseText(string $data): string
    {
        $temp = self::getTempFile();
        file_put_contents($temp, gzdecode($data));

        try {
            $result = Parser::parse($data);
            unlink($temp);
            return $result;
        } catch (\Exception $ex) {
            unlink($temp);
            throw $ex;
        }
    }
}
