<?php

namespace TextMedia\FileParser\Parser;

/**
 * Парсер RTF
 */
class Rtf extends AbstractConsole
{
    /**
     * Параметры парсера
     *
     * @return array
     */
    protected static function getParams(): array
    {
        return [
            'unrtf',
            '$command $file',
            function ($text) {
                return self::getConverter()->encode(
                    html_entity_decode(strip_tags($text), ENT_QUOTES, 'cp1252'),
                    'UTF-8',
                    'Windows-1251'
                );
            },
        ];
    }
}
