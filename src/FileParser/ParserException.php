<?php

namespace TextMedia\FileParser;

/**
 * Собственные исключения для пакета.
 */
class ParserException extends \Exception
{
    /** Ошибки при работе с файловой системой. */
    const FILESYS_ERROR = 0xff01;

    /** Ошибки при работе с архивами. */
    const ARCHIVE_ERROR = 0xff02;

    /** Ошибки при работе с консольными утилитами. */
    const CONSOLE_ERROR = 0xff03;

    /** Ошибки обработки содержимого файла. */
    const PARSE_ERROR = 0xff04;

    /** Ошибки при скачивании файла по ссылке. */
    const URL_ERROR = 0xff05;

    /** Прочие ошибки. */
    const MISC_ERROR = 0xff06;
}
