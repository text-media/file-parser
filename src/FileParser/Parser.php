<?php

namespace TextMedia\FileParser;

use \TextMedia\MimeTypes\MimeTypes;

use \ReflectionClass;

/**
 * Получение текста из файлов различных типов.
 */
class Parser extends Parser\AbstractCommon
{
    /** Microsoft Word (закрытый формат). */
    const MSWORD = 'doc';

    /** Microsoft Word (OpenXML-формат). */
    const MSWORD_OPENXML = 'docx';

    /** Electronic Publication */
    const ELECTRONIC_PUBLICATION = 'epub';

    /** Fiction Book 2.*. */
    const FICTION_BOOK = 'fb2';

    /** GNU Zip */
    const GNU_ZIP = 'gz';

    /** HyperText Markup Language. */
    const HYPERTEXT_MARKUP = 'html';

    /** OpenDocument (LibreOffice, OpenOffice). */
    const OPEN_DOCUMENT = 'odt';

    /** Portable Document Format */
    const PORTABLE_DOCUMENT = 'pdf';

    /** Rich Text Format (WordPad, Word-мамонт). */
    const RICH_TEXT = 'rtf';

    /** Обычный текст. */
    const PLAIN_TEXT = 'txt';

    /** ZIP-архив. */
    const ZIP_ARCHIVE = 'zip';

    /** Jpeg-изображение. */
    const JPEG_IMAGE = 'jpeg';

    /** Png-изображение. */
    const PNG_IMAGE = 'png';

    /** Gif-изображение. */
    const GIF_IMAGE = 'gif';

    /** Bmp-изображение. */
    const BMP_IMAGE = 'bmp';

    /** Электронная таблица Xlsx. */
    const XLSX_SPREADSHEET = 'xlsx';

    /** Электронная таблица Xls. */
    const XLS_SPREADSHEET = 'xls';

    /** Электронная таблица Ods. */
    const ODS_SPREADSHEET = 'ods';

    /** URL. */
    const URL = 'url';

    /** @var array Список поддерживаемых типов и классов-парсеров. */
    private static $types = null;

    /**
     * Парсинг файла.
     *
     * @param string  $source Путь к файлу или данные из него.
     * @param string  $type   OPTIONAL Тип файла. Если не указан - определяется автоматически.
     * @param boolean $file   OPTIONAL Передан путь к файлу (по умолчанию) или данные.
     *
     * @return string
     */
    public static function parse(string $source, string $type = null, bool $file = true): string
    {
        // если переданы данные, а не файл, и не указано расширение,
        // попробуем сохранить файл во временную папку и обработать его
        if (empty($type) and !$file) {
            $temp = self::getTempFile();
            file_put_contents($temp, $source);
            try {
                $result = call_user_func(__METHOD__, $temp);
                unlink($temp);
                return $result;
            } catch (\Exception $ex) {
                unlink($temp);
                throw $ex;
            }
        } elseif (self::URL === $type or ($file and Parser\Url::checkUrlValid($source))) {
            // если передан URL - получим содержимое по указанному адресу
            return Parser\Url::parse($source);
        }

        $parser = empty($type)
            ? self::getParserByFile($source)
            : self::getParserByType((string) $type);
        return call_user_func([$parser, __FUNCTION__], $source, $file);
    }

    /**
     * Получение списка доступных для парсинга типов файлов.
     *
     * @return array
     */
    public static function getAvailableTypes(): array
    {
        if (is_null(self::$types)) {
            self::$types = [];
            foreach (glob(__DIR__ . '/Parser/*.php') as $file) {
                $type   = substr(basename($file), 0, -4);
                $parser = __NAMESPACE__ . '\\Parser\\' . $type;
                if (self::checkParserName($parser)) {
                    self::$types[strtolower($type)] = $parser;
                }
            }
        }
        return array_keys(self::$types);
    }

    /**
     * Получение списка доступных для парсинга mime-типов файлов.
     *
     * @return array
     */
    public static function getAvailableMimes(): array
    {
        static $mimeTypes = null;
        if (is_null($mimeTypes)) {
            $mimeTypes = [];
            foreach (self::getAvailableTypes() as $type) {
                $mimeTypes = array_merge($mimeTypes, MimeTypes::getMimesByExtension($type));
            }
        }
        return $mimeTypes;
    }

    /**
     * Получение парсера по указанному типу файла.
     * Метод вспомогательный, но может использоваться для проверки, а возможно ли вообще обработать данный тип файлов.
     *
     * @param string $type Тип файла.
     *
     * @staticvar array $typeAliases Алиасы типов (расширения файлов с одним и тем же mime-типом).
     *
     * @throws \TextMedia\FileParser\ParserException Если не удалось определить имя парсера.
     *
     * @return string
     */
    public static function getParserByType(string $type): string
    {
        static $typeAliases = [];

        // проверим алиасы
        $type = strtolower($type);
        if (!array_key_exists($type, $typeAliases)) {
            foreach (MimeTypes::getMimesByExtension($type) as $mime) {
                if (false !== ($typeAliases[$type] = self::getBaseExtension($mime))) {
                    break;
                }
            }
        }
        if (!empty($typeAliases[$type])) {
            $type = $typeAliases[$type];
        }

        // найдём парсер
        if (!array_key_exists($type, self::$types)) {
            throw new ParserException("Неподдерживаемый тип файла: {$type}.", ParserException::PARSE_ERROR);
        }

        return self::$types[$type];
    }

    /**
     * Получение парсера по файлу (расширению, содержимому).
     * Метод вспомогательный, но может использоваться для проверки, а возможно ли вообще обработать данный файл.
     *
     * @param string $mime Mime-тип файла.
     *
     * @throws \TextMedia\FileParser\ParserException Если не удалось определить тип.
     *
     * @return string
     */
    public static function getParserByMime(string $mime): string
    {
        $type = self::getBaseExtension($mime);

        if (empty($type)) {
            throw new ParserException("Не удалось определить тип файла.", ParserException::FILESYS_ERROR);
        }

        return self::getParserByType($type);
    }

    /**
     * Получение парсера по файлу (расширению, содержимому).
     * Метод вспомогательный, но может использоваться для проверки, а возможно ли вообще обработать данный файл.
     *
     * @param string  $file    Путь к файлу.
     * @param boolean $extOnly OPTIONAL Только по расширению.
     *
     * @throws \TextMedia\FileParser\ParserException Если не удалось определить тип.
     *
     * @return string
     */
    public static function getParserByFile(string $file, bool $extOnly = false): string
    {
        if (!$extOnly) { // сперва по mime-типу, если разрешено
            self::checkFile($file);
            $mime = MimeTypes::getFileMimeType($file);
            try {
                return self::getParserByMime($mime);
            } catch (\Exception $ex) {
                unset($ex); // nothing to do
            }
        }

        // потом - по расширению
        $type = MimeTypes::getFileExtension($file);

        if (empty($type)) { // нет - исключение
            throw new ParserException("Не удалось определить тип файла: {$file}.", ParserException::FILESYS_ERROR);
        }

        return self::getParserByType($type);
    }

    /**
     * Получение типа парсера (расширения файла) по его названию.
     *
     * @param string $parser Тип парсера (имя класса).
     *
     * @throws \TextMedia\FileParser\ParserException Если имя парсера - неправильное.
     *
     * @return string
     */
    public static function getParserTypeName(string $parser): string
    {
        if (!self::checkParserName($parser)) {
            throw new ParserException("Неправильное имя парсера: {$file}.", ParserException::MISC_ERROR);
        }

        return strtolower(substr($parser, 1 + strrpos($parser, '\\')));
    }

    /**
     * Получение основного расширения по mime-типу.
     *
     * @param string $mime mime-тип.
     *
     * @return string|FALSE
     */
    public static function getBaseExtension(string $mime)
    {
        $types = array_intersect(MimeTypes::getExtenstionsByMime($mime), self::getAvailableTypes());
        return (0 !== count($types)) ? reset($types) : false;
    }

    /**
     * Исправление имени файла в соответствии с правильным расширением.
     *
     * @param string $file Исходное имя файла.
     * @param string $ext  Правильное расширение.
     */
    public static function fixFileName(string $file, string $ext): string
    {
        $extValid = strtolower($ext);
        $extCurr  = MimeTypes::getFileExtension($file);
        if ($extCurr !== $extValid) {
            if (self::GNU_ZIP === $extValid) {
                $file .= ".{$extValid}";
            } else {
                $file = substr($file, 0, -strlen($extCurr)) . $extValid;
            }
        }
        return $file;
    }

    /**
     * Проверкра имени парсера.
     *
     * @param string $parser Парсер (имя класса).
     *
     * @return boolean
     */
    public static function checkParserName(string $parser): bool
    {
        return (class_exists($parser)
            and is_subclass_of($parser, Parser\AbstractParser::class)
            and !(new ReflectionClass($parser))->isAbstract());
    }
}
