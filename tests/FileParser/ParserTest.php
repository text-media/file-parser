<?php

namespace TextMedia\FileParser\Tests;

use \TextMedia\FileParser\Parser;

use \SplFileInfo;

/**
 * Тесты.
 *
 * Папка Resources должна содержать пары файлов: проверяемый + ожидаемый результат.
 * Подребнее см. в README.md
 */
class ParserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Проверка разбора файла
     *
     * @param string $file Файл для парсинга
     * @param string $hash Хэш чистого текста
     *
     * @dataProvider fileProvider
     */
    public function testParser(string $file, string $hash)
    {
        // парсим файл
        $fileText = Parser::parse($file);
        $fileHash = md5($fileText);

        // парсим данные
        $fileData = file_get_contents($file);
        $dataText = Parser::parse($fileData, null, false);
        $dataHash = md5($dataText);

        // сравним оба хэша с эталоном
        $this->assertEquals($fileHash, $hash);
        $this->assertEquals($dataHash, $hash);
    }

    /**
     * @return array
     */
    public function fileProvider(): array
    {
        $exts = Parser::getAvailableTypes();
        return array_filter(array_map(function ($hash) use ($exts) {
            $file = substr($hash, 0, -4);
            $type = (new SplFileInfo($file))->getExtension();
            return (in_array($type, $exts) and is_file($file))
                ? [$file, file_get_contents($hash)]
                : false;
        }, glob(__DIR__ . '/Resources/*.md5')));
    }
}
