# Тесты

В корне проекта выполните:

```bash
./vendor/bin/phpunit -c ./
```

Файлы для тестов находятся в папке `tests/FileParser/Resources`.
Т.к. они занимают много места - каждый из тестируемых файлов следует сжимать командой `gzip -9 …`.
Рядом с каждым тестируемым файлом должен находиться ещё один, содержащий `md5`-хэш чистого текста.
Их имена должны иметь следующий вид, например: `test4.pdf.gz` и `test4.pdf.gz.md5`.

Имеющиеся файлы для тестов взяты реальные - те, которые загружали нам на проверку пользователи; HTML - те, по которым ранее создавались задачи:

 - `test.html.gz`:  https://github.com/stigmat/text.ru/issues/3129
 - `test.zip`:      https://github.com/stigmat/text.ru/issues/3161
 - `test2.html.gz`: https://github.com/stigmat/text.ru/issues/3058
 - `test3.html.gz`: https://github.com/stigmat/text.ru/issues/2977
