# File Parser

[![Packagist](https://img.shields.io/packagist/v/text-media/file-parser.svg)](https://packagist.org/packages/text-media/file-parser)
[![Packagist](https://img.shields.io/packagist/l/text-media/file-parser.svg)](https://packagist.org/packages/text-media/file-parser)

Получение текста из файлов различных типов.

## Установка

Установить все необоходимые консольные утилиты (если чего не будет хватать, установщик предупредит, что и как установить):

```bash
sudo apt-get install antiword unrtf poppler-utils
```

Добавить в composer.json зависимость:

```bash
composer require text-media/file-parser
```

Правки в `composer.json` для автопроверки наличия необходимых консолных утилит:

```json
{
    "scripts": {
        "post-install-cmd": ["file-parser-check-commands.php"],
        "post-update-cmd":  ["file-parser-check-commands.php"]
    }
}
```

По умолчанию будут проверены все утилиты, прописанные в `file-parser-check-commands.php`.
Если нужно проверить какие-то конкретные - в конце необходимо перечислить типы файлов.
Для исключения какой-либо команды её нужно указать с префиксом "-". Например:

```json
{
    "scripts": {
        "post-install-cmd": ["file-parser-check-commands.php doc rtf -url"],
        "post-update-cmd":  ["file-parser-check-commands.php doc rtf -url"]
    }
}
```

## Использование

Пример использования:

```php
use \TextMedia\FileParser\Parser;
use \TextMedia\FileParser\ParserException;
use \TextMedia\FileParser\Parser\Parser\Pdf;

// получение списка поддерживаемых типов файлов
$types = Parser::getAvailableTypes();

// парсинг с авто-определением типа
$text = Parser::parse('/tmp/test.rtf');

// парсинг с явным указанием типа
$text = Parser::parse('/tmp/uploaded_file', 'docx');

// парсинг заранее прочитанного из файла текста
// тип - желателен, т.к. может оказаться не определён автоматически
$text = Parser::parse('<html>…</html>', 'html', false);

// явное указание парсера
$text = Pdf::parse(file_get_contents($pdf), false);

// проверка возможности парсинга файла указанного типа
try {
    $parser = Parser::getParserByType('htm');
    $text = $parser::parse($file);
} catch (ParserException $e) {
    // …
}

// проверка возможности парсинга указанного файла
try {
    $parser = Parser::getParserByFile($file);
    $text = $parser::parse($file);
} catch (ParserException $e) {
    // …
}
```

Поддерживается парсинг сжатых при помощи `gzip` и `zip` файлов: архив должен содержать только один файл, который будет распакован во временную директорию, после чего к нему будет применён метод `Parser::parseFile` с автоопределением типа.

## Добавление типов

Для добавления нового типа парсера достаточно создать новый класс в `src/FileParser/Parser`, унаследовав его от `AbstractParser` (или его абстрактных потомков).

`composer` не поддерживает установку зависимостей от консольных утилит - для их проверки используется скрипт `scripts/file-parser-check-commands.php.php`.
При добавлении нового парсера, если для его работы требуется дополнительная консольная утилита, её необходимо прописать в `$checkCommands` этого скрипта.
