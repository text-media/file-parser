#!/usr/bin/env php
<?php

/**
 * Вывод ошибки и завершение скрипта
 *
 * @param string $message   Текст ошибки
 * @param string $important Важный текст
 */
$throwError = function (string $message, string $important) {
    printf("\e[41;97m%s: \e[1m%s\e[0m\n", $message, $important) and exit(0xff);
};

// проверим сперва доступ на запись во временную папку
$tempDir = new SplFileInfo(sys_get_temp_dir());
if (false === $tempDir->getRealPath() or !$tempDir->isDir() or !$tempDir->isWritable()) {
    $throwError('Папка временных файлов должны быть доступна для записи', (string) $tempDir);
}

/** @var array $commands Список утилит: [тип файла, консольная команда, описание установки] */
$checkCommands = [
    ['doc',  'antiword',  'sudo apt-get install antiword'],
    ['pdf',  'pdftotext', 'sudo apt-get install poppler-utils'],
    ['rtf',  'unrtf',     'sudo apt-get install unrtf'],
    ['url',  'phantomjs', 'http://phantomjs.org/build.html'],
    ['jpeg', 'tesseract', 'sudo apt-get install tesseract-ocr tesseract-ocr-eng tesseract-ocr-rus'],
    ['png',  'tesseract', 'sudo apt-get install tesseract-ocr tesseract-ocr-eng tesseract-ocr-rus'],
    ['gif',  'tesseract', 'sudo apt-get install tesseract-ocr tesseract-ocr-eng tesseract-ocr-rus'],
    ['bmp',  'tesseract', 'sudo apt-get install tesseract-ocr tesseract-ocr-eng tesseract-ocr-rus'],
];

// Получим из командной строки список типов файлов,
// для которых необходимо проверить наличие консольных утилит.
// Если список пуст - проверяются всё, перечисленное в $checkCommands.
$include = $exclude = [];
foreach (array_splice($argv, 1) as $command) {
    if ('-' === $command{0}) {
        array_push($exclude, substr($command, 1));
    } else {
        array_push($include, $command);
    }
}
$checkTypes = array_diff($include ?: array_map('reset', $checkCommands), $exclude);

// проверим все утилиты и завершим скрипт кодом 0, если всё ОК
array_walk($checkCommands, function ($item) use ($throwError, $checkTypes) {
    list($type, $command, $install) = $item;
    if (in_array($type, $checkTypes)) {
        if (!preg_match("#{$command}:\s*(\S+)#", `whereis $command`)) {
            $throwError("Для парсинга '{$type}' необходимо установить '{$command}'", $install);
        }
    }
}) and exit(0);
